import RoleGame.*
import org.junit.Test
import org.junit.Assert

//----------------------------------------------------------------------------------------------------------------------
// TESTS:
//----------------------------------------------------------------------------------------------------------------------

class Tests {

    @Test
    fun createCharacter() {
        var character = createDafaultCharacter()
        Assert.assertEquals(1000.0, character.health, 0.001)
        Assert.assertEquals(1, character.level)
        Assert.assertEquals(true, character.isAlive())
    }

    @Test
    fun damageCharacter() {
        var attacker = createDafaultCharacter()
        var target = createDafaultCharacter()
        attack(Scene(), attacker, target, 10.0, AttackRanges.Melee)
        Assert.assertEquals(990.0, target.health, 0.001 )
    }

    @Test
    fun damageCharacterOverflow() {
        var character = createDafaultCharacter()
        character.health = 2000.0
        Assert.assertEquals(1000.0, character.health, 0.001)
    }

    @Test
    fun healCharacter() {
        val character = createDafaultCharacter()
        character.health = 600.0;
        Assert.assertEquals(600.0, character.health, 0.001);
    }

    @Test
    fun healCharacterOverflow() {
        var character = createDafaultCharacter()
        character.health = 500.0
        character.health = 2000.0
        Assert.assertEquals(1000.0, character.health, 0.001)
    }

    @Test
    fun healDeadCharacter() {
        var character = createDafaultCharacter()
        character.health = 0.0
        character.health = 200.0
        Assert.assertEquals(0.0, character.health, 0.001)
    }

    @Test
    fun characterCanNotDealDamageItself() {
        var character = createDafaultCharacter()
        val initialHealth = character.health
        val damage = 200.0
        attack(Scene(), character, character, damage, AttackRanges.Melee)
        Assert.assertEquals(initialHealth, character.health, 0.001)
    }

    @Test
    fun characterCanOnlyHealItself() {
        var character = createDafaultCharacter()
        character.health = 600.0
        val initialHealth = character.health
        val amount = 200.0
        heal(Scene(), character, character, amount, AttackRanges.Melee)
        Assert.assertEquals(initialHealth, character.health, 0.001)
    }

    @Test
    fun if_the_target_is_5_or_more_Levels_above_the_attacker_Damage_is_reduced_by_50_percent() {
        var attacker = createDafaultCharacter()
        var target = createDafaultCharacter()
        attacker.level = 10
        target.level = 20
        val initialHealth = target.health
        val damage = 20.0
        attack(Scene(), attacker, target, damage, AttackRanges.Melee)
        Assert.assertEquals(initialHealth - damage / 2.0, target.health, 0.001)
    }

    @Test
    fun if_the_target_is_5_or_more_Levels_below_the_attacker_Damage_is_increased_by_50_percent() {
        var attacker = createDafaultCharacter()
        var target = createDafaultCharacter()
        attacker.level = 20
        target.level = 10
        val initialHealth = target.health
        val damage = 20.0
        attack(Scene(), attacker, target, damage, AttackRanges.Melee)
        Assert.assertEquals(initialHealth - damage * 1.5, target.health, 0.001)
    }

    @Test
    fun `damage preserved if characters are within +-5 level`() {
        var attacker = createDafaultCharacter()
        var target = createDafaultCharacter()
        var initialHealth = target.health
        val damage = 20.0

        attacker.level = 12
        target.level = 10
        attack(Scene(), attacker, target, damage, AttackRanges.Melee)
        Assert.assertEquals(initialHealth - damage, target.health, 0.001)

        initialHealth = target.health
        attacker.level = 10
        target.level = 12
        attack(Scene(), attacker, target, damage, AttackRanges.Melee)
        Assert.assertEquals(initialHealth - damage, target.health, 0.001)
    }


    @Test
    fun `Melee fighters have a range of 2 meters`() {
        val attacker = createMeleeCharacter()
        val target = createMeleeCharacter()
        val scene = Scene()
        val damage = 50.0
        //Attack Out of range:
        var initialHealth = target.health
        attack(scene, attacker, target, damage, 4.0)
        Assert.assertEquals(initialHealth, target.health, 0.001)
        //Attack In range:
        initialHealth = target.health
        attack(scene, attacker, target, damage, 1.5)
        Assert.assertEquals(initialHealth-damage, target.health, 0.001)
    }

    @Test
    fun `Ranged fighters have a range of 20 meters`() {
        val attacker = createRangedCharacter()
        val target = createMeleeCharacter()
        val scene = Scene()
        val damage = 50.0
        val initialHealth = target.health
        //Attack Out of range:
        attack(scene, attacker, target, damage, 28.0)
        Assert.assertEquals(initialHealth, target.health, 0.001)
        //Attack In range:
        attack(scene, attacker, target, damage, 15.0)
        Assert.assertEquals(initialHealth-damage, target.health, 0.001)
    }

    @Test
    fun `Players belonging to the same Faction are considered Allies`() {
        //Es necesario testear estos casos?
    }

    @Test
    fun `Allies cannot Deal Damage to one another`() {
        val playerA = createMeleeCharacterFactionAlliance()
        val playerB = createMeleeCharacterFactionAlliance()
        val initialHealth = playerB.health
        val damage = 50.0
        attack(Scene(), playerA, playerB, damage, AttackRanges.Melee)
        Assert.assertEquals(initialHealth, playerB.health, 0.001)
    }

    @Test
    fun `Distinct faction players can deal Damage to one another`() {
        val playerA = createMeleeCharacterFactionAlliance()
        val playerB = createMeleeCharacterFactionHorde()
        val initialHealth = playerB.health
        val damage = 50.0
        attack(Scene(), playerA, playerB, damage, AttackRanges.Melee)
        Assert.assertEquals(initialHealth-damage, playerB.health, 0.001)
    }

    @Test
    fun `Allies can Heal one another`() {
        val playerA = createMeleeCharacterFactionAlliance()
        val playerB = createMeleeCharacterFactionAlliance()
        playerB.health = 600.0
        val initialHealth = playerB.health
        val heal = 50.0
        heal(Scene(), playerA, playerB, heal, AttackRanges.Melee)
        Assert.assertEquals(initialHealth+heal, playerB.health, 0.001)
    }

    @Test
    fun `Distinct Factions Can Not Heal one another`() {
        val playerA = createMeleeCharacterFactionAlliance()
        val playerB = createMeleeCharacterFactionHorde()
        playerB.health = 600.0
        val initialHealth = playerB.health
        val heal = 50.0
        heal(Scene(), playerA, playerB, heal, AttackRanges.Melee)
        Assert.assertEquals(initialHealth, playerB.health, 0.001)
    }

    @Test
    fun `These things cannot be Healed and they do not Deal Damage`(){
        val playerA = createMeleeCharacter()
        val tree = createTree()
        tree.health = 600.0
        val initialHealth = tree.health
        val heal = 50.0
        heal(Scene(), playerA, tree, heal, AttackRanges.Melee)
        Assert.assertEquals(initialHealth, tree.health, 0.001)
    }

    @Test
    fun `These things do not belong to Factions they are neutral`(){
        val rock = createStatic()
        Assert.assertTrue(rock.isNeutral())
    }

    @Test
    fun `When reduced to 0 Health things are Destroyed`(){
        val playerA = createMeleeCharacter()
        val tree = createTree()
        val damage = tree.health*2.0
        attack(Scene(), playerA, tree, damage, AttackRanges.Melee)
        Assert.assertTrue(tree.isDestroyed())
    }

    @Test
    fun `When reduced above 0 Health things are not Destroyed yet`(){
        val playerA = createMeleeCharacter()
        val tree = createTree()
        val damage = tree.health/2.0
        attack(Scene(), playerA, tree, damage, AttackRanges.Melee)
        Assert.assertFalse(tree.isDestroyed())
    }
}








