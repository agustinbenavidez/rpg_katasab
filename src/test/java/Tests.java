//import org.junit.Assert;
//import org.junit.Test;
///*
//interface GameEntity{
//    public void setHealth(int health) {
//        this.health = health;
//    }
//    public int getLevel() {
//        return level;
//    }
//}
//*/
//
//class Character {
//    private final int maxHealth = 1000;
//    private final int minHealth = 0;
//    private int health = maxHealth;
//    private int level = 1;
//
//    public Character() {
//    }
//
//    public int getHealth() {
//        return health;
//    }
//
//    public void setHealth(int health) {
//        this.health = health;
//    }
//
//    public int getLevel() {
//        return level;
//    }
//
//    public void setLevel(int level) {
//        this.level = level;
//    }
//
//    public boolean isAlive() {
//        return health > minHealth;
//    }
//
//    public void damage(int ammount) {
//        this.health = Math.max(minHealth, this.health - ammount);
//    }
//
//    public void heal(int ammount) {
//        if (this.health == 0) return;
//        this.health = Math.min(maxHealth, this.health + ammount);
//    }
//}
//
//class World {
//
//    static public double damageModifierByLevel(int dealerLevel, int targetLevel) {
//        final double fiftyPercentMoreDamage = 1.5;
//        final double halfDamage = 0.5;
//        final double sameDamage = 1.0;
//        if ((dealerLevel - targetLevel) >= 5) {
//            return fiftyPercentMoreDamage;
//        } else if ((targetLevel - dealerLevel) >= 5) {
//            return halfDamage;
//        } else {
//            return sameDamage;
//        }
//    }
//
//    static public void damageEvent(Character dealer, Character target, int amount) {
//        if (dealer == target) {
//            return;
//        }
//        double finalAmount = amount * damageModifierByLevel(dealer.getLevel(), target.getLevel());
//        target.damage((int) finalAmount);
//    }
//    /*
//    static public <T extends > void damageEventG(T dealer, T target, int amount) {
//        if (dealer == target) {
//            return;
//        }
//        double finalAmount = amount * damageModifierByLevel(dealer.getLevel(), target.getLevel());
//        target.damage((int) finalAmount);
//    }
//    */
//    static public void healEvent(Character healer, Character healed, int amount) {
//        if (healer != healed) {
//            return;
//        }
//        healed.heal(amount);
//    }
//
//}
//
//public class Tests {
//
//    private Character setupCharacter() {
//        Character character = new Character();
//        return character;
//    }
//
//
//    @Test
//    public void createCharacter() {
//        Character character = setupCharacter();
//        Assert.assertEquals(1000, character.getHealth());
//        Assert.assertEquals(1, character.getLevel());
//        Assert.assertEquals(true, character.isAlive());
//    }
//
//    @Test
//    public void damageCharacter() {
//        Character character = setupCharacter();
//        character.damage(10);
//        Assert.assertEquals(990, character.getHealth());
//    }
//
//    @Test
//    public void damageCharacterOverflow() {
//        Character character = setupCharacter();
//        character.damage(2000);
//        Assert.assertEquals(0, character.getHealth());
//    }
//
//    @Test
//    public void healCharacter() {
//        Character character = setupCharacter();
//        character.setHealth(600);
//        character.heal(300);
//        Assert.assertEquals(900, character.getHealth());
//    }
//
//    @Test
//    public void healCharacterOverflow() {
//        Character character = setupCharacter();
//        character.setHealth(600);
//        character.heal(500);
//        Assert.assertEquals(1000, character.getHealth());
//    }
//
//    @Test
//    public void healDeadCharacter() {
//        Character character = setupCharacter();
//        character.setHealth(0);
//        character.heal(300);
//        Assert.assertEquals(0, character.getHealth());
//    }
//
//    @Test
//    public void characterCanNotDealDamageItself() {
//        Character character1 = setupCharacter();
//        int initialHealth = character1.getHealth();
//        int amount = 100;
//        World.damageEvent(character1, character1, amount);
//        Assert.assertEquals(initialHealth, character1.getHealth());
//    }
//
//    @Test
//    public void characterCanOnlyHealItself() {
//        Character character = setupCharacter();
//        int initHealth = 900;
//        character.setHealth(initHealth);
//        int amount = 50;
//        World.healEvent(character, character, amount);
//        Assert.assertEquals(initHealth + amount, character.getHealth());
//        /*
//        Character character = setupCharacter();
//        int damageAmount = 50;
//        character.damage(damageAmount);
//        int health = character.getHealth();
//        int healAmount = 50;
//        World.healEvent(character, character, healAmount);
//        Assert.assertEquals(health + healAmount ,character.getHealth());
//     */
//    }
//
//    @Test
//    public void if_the_target_is_5_or_more_Levels_above_the_attacker_Damage_is_reduced_by_50_percent() {
//        Character attacker = setupCharacter();
//        Character attacked = setupCharacter();
//        attacker.setLevel(10);
//        attacked.setLevel(20);
//        int initHealth = attacked.getHealth();
//        int damage = 20;
//        World.damageEvent(attacker, attacked, damage);
//        int expectedHealth = initHealth - damage / 2;
//        Assert.assertEquals(expectedHealth, attacked.getHealth());
//    }
//
//    @Test
//    public void if_the_target_is_5_or_more_Levels_below_the_attacker_Damage_is_increased_by_50_percent() {
//        Character attacker = setupCharacter();
//        Character attacked = setupCharacter();
//        attacker.setLevel(20);
//        attacked.setLevel(10);
//        int initHealth = attacked.getHealth();
//        int damage = 20;
//        World.damageEvent(attacker, attacked, damage);
//        int expectedHealth = initHealth - (int)(damage * 1.5);
//        Assert.assertEquals(expectedHealth, attacked.getHealth());
//    }
//
//    @Test
//    public void if_the_target_has_the_same_level_than_the_the_attacker_Damage_is_not_altered() {
//        Character attacker = setupCharacter();
//        Character attacked = setupCharacter();
//        int initHealth = attacked.getHealth();
//        int damage = 20;
//        World.damageEvent(attacker, attacked, damage);
//        int expectedHealth = initHealth - damage;
//        Assert.assertEquals(expectedHealth, attacked.getHealth());
//    }
//}
