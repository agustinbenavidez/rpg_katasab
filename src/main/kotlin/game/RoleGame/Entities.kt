package RoleGame
import Engine.*

// Constants:
enum class FactionType { Neutral, Alliance, Horde }
enum class CategoryType { Player, Static }

class AttackRanges{companion object{
    val Melee = 2.0
    val Ranged = 20.0
}}


// Entities:
class RoleGameEntity(
        attackRange : Double = AttackRanges.Melee,
        var factions: MutableList<FactionType> = mutableListOf(FactionType.Neutral),
        var category : CategoryType = CategoryType.Player

) : GameEntity(attackRange = attackRange){

    fun isDestroyed() : Boolean = (health==0.0) && (category==CategoryType.Static)
    fun isNeutral()   : Boolean = factions.contains(FactionType.Neutral)  &&  factions.size == 1
}

class Scene : Context