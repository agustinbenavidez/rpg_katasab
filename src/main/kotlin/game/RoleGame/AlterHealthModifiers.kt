package RoleGame
import Engine.*

fun inflicterLevelIsMoreThanFive(computedAmount : Double, scene : Scene, inflicter : GameEntity, target : GameEntity, amount : Double) : Double
    = if ((inflicter.level - target.level) >= 5)
        computedAmount * 1.5
    else
        computedAmount

fun targetLevelIsMoreThanFive(computedAmount: Double, scene : Scene, inflicter : GameEntity, target : GameEntity, amount : Double) : Double
    = if ((target.level - inflicter.level) >= 5)
        computedAmount / 2.0
    else
        computedAmount



