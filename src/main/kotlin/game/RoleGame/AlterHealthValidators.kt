package RoleGame

fun isNotSelfInflicting(inflicter : RoleGameEntity, target : RoleGameEntity, distance : Double, scene : Scene) : Boolean
    = inflicter != target

fun inRange(attacker : RoleGameEntity, target : RoleGameEntity, distance : Double, scene : Scene)  : Boolean
    = attacker.attackRange >= distance

fun isInflicterPlayerType(inflicter : RoleGameEntity, target : RoleGameEntity, distance : Double, scene : Scene)  : Boolean
    = inflicter.category == CategoryType.Player

fun isTargetPlayerType(inflicter : RoleGameEntity, target : RoleGameEntity, distance : Double, scene : Scene)  : Boolean
    = target.category == CategoryType.Player

fun areInTheSameFaction(inflicter : RoleGameEntity, target : RoleGameEntity, distance : Double, scene : Scene)  : Boolean
    = inflicter.factions.intersect(target.factions).isNotEmpty()

fun areInDifferentFaction(inflicter : RoleGameEntity, target : RoleGameEntity, distance : Double, scene : Scene)  : Boolean
    = (target.factions.size==1 && target.factions.contains(FactionType.Neutral)) ||
           (inflicter.factions.intersect(target.factions).isEmpty()) ||
           (inflicter.category != target.category)
