package RoleGame

fun createDafaultCharacter()                = RoleGameEntity()
fun createMeleeCharacter()                  = RoleGameEntity(attackRange = AttackRanges.Melee)
fun createRangedCharacter()                 = RoleGameEntity(attackRange = AttackRanges.Ranged)
fun createRangedCharacterFactionAlliance()  = RoleGameEntity(attackRange = AttackRanges.Ranged, factions = mutableListOf(FactionType.Alliance))
fun createMeleeCharacterFactionAlliance()   = RoleGameEntity(attackRange = AttackRanges.Melee,  factions = mutableListOf(FactionType.Alliance))
fun createRangedCharacterFactionHorde()     = RoleGameEntity(attackRange = AttackRanges.Ranged, factions = mutableListOf(FactionType.Horde))
fun createMeleeCharacterFactionHorde()      = RoleGameEntity(attackRange = AttackRanges.Melee,  factions = mutableListOf(FactionType.Horde))
fun createTree()                            = RoleGameEntity(category = CategoryType.Static)
fun createStatic()                          = RoleGameEntity(category = CategoryType.Static)