package RoleGame
import Engine.*

// Interactions:

fun attack(scene : Scene, attacker : RoleGameEntity, target : RoleGameEntity, amount : Double, distance : Double){
    val validators =  arrayOf<AlterHealthValidators<RoleGameEntity, Scene>>(
            ::isNotSelfInflicting,
            ::inRange,
            ::areInDifferentFaction)
    val damageModifiers =  arrayOf<AlterHealthModifiers<RoleGameEntity, Scene>>(
            ::inflicterLevelIsMoreThanFive,
            ::targetLevelIsMoreThanFive)

    alterHealth(scene, attacker, target, -1.0*amount, distance, validators, damageModifiers)
}


fun heal(scene : Scene, healer : RoleGameEntity, target : RoleGameEntity, amount : Double, distance : Double){
    val validators =  arrayOf<AlterHealthValidators<RoleGameEntity, Scene>>(
            ::isNotSelfInflicting,
            ::inRange,
            ::isInflicterPlayerType,
            ::isTargetPlayerType,
            ::areInTheSameFaction)

    alterHealth(scene, healer, target, amount, distance, validators, arrayOf())
}




