package Engine


open class GameEntity(
        var name : String = "",
        var level : Int = 1,
        health : Double = 1000.0,
        var minHealth : Double = 0.0,
        var maxHealth : Double = 1000.0,
        var healthSetter : HealthSetter = ::healthSetter,
        var attackRange : Double = 2.0
){
    var health : Double = health
        set(amount){ field = healthSetter(health, maxHealth, minHealth, amount) }

    fun isAlive() = health > minHealth
}

fun healthSetter(health: Double, maxHealth: Double, minHealth: Double, ammount : Double) : Double =
        if (health > 0.0) Math.max(minHealth, Math.min(maxHealth, ammount)) else health

