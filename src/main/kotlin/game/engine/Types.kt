package Engine

// Types:
typealias HealthSetter = (health: Double, maxHealth: Double, minHealth: Double, ammount : Double)-> Double
typealias AlterHealthModifiers<T, U> = (health:Double, context:U, inflicter:T, target:T, amount:Double) -> Double
typealias AlterHealthValidators<T, U> = (inflicter:T, target:T, distance:Double, context:U) -> Boolean

