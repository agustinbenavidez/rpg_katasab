package Engine


fun<T:GameEntity, U:Context> alterHealth(context : U, inflicter : T, target : T, amount : Double, distance : Double, alterHealthValidators : Array<AlterHealthValidators<T,U>>, alterHealthModifiers : Array<AlterHealthModifiers<T,U>>){
    // Validate if the interaction may go on:
    for (validator in alterHealthValidators){
        if( !validator(inflicter, target, distance, context) ){
            return
        }
    }
    // Apply health modifiers:
    var computedAmount = amount
    for (modifier in alterHealthModifiers){
        computedAmount = modifier(computedAmount, context, inflicter, target, amount)
    }
    // Commit health:
    target.health += computedAmount
}
